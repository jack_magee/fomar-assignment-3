#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include "vec2.h"
#include "vec3.h"

using namespace std;

float distance(vec2<float> point, vec2<float> A, vec2<float> B)
{
    float top = (B.x() - A.x())*(A.y()-point.y()) - (A.x()- point.x())*(B.y() - A.y());
    float bot = sqrt((B.x()-A.x())*(B.x()-A.x()) + (B.y()-A.y())*(B.y()-A.y()));
    return top/bot;
}

int main(int argc, char** argv)
{
    int nx = 128;
    int ny = 128;
    vec2<float> A = vec2<float>(61,10);
    vec2<float> B = vec2<float>(100,100);
    vec2<float> C = vec2<float>(25,90);
    vec3<vec2<float>> triangle = vec3<vec2<float>>(A,B,C);

    ofstream fout;
    fout.open("halfplane.ppm");
    fout << "P3\n" << nx << " " << ny << "\n255\n";

    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
            vec2<float> coords = vec2<float>(i,j);
            int r,g,b;
            float alpha = (distance(coords, C,B) / distance(A, C,B));
            float beta = (distance(coords, A,C) / distance(B, A,C));
            float gamma = (distance(coords, B,A) / distance(C, B,A));
            if (alpha < 0){
                r = 0;
            }
            if (alpha >= 0){
                r = 255;
            }
            if (beta < 0){
                g = 0;
            }
            if (beta >= 0){
                g = 255;
            }
            if (gamma < 0){
                b = 0;
            }
            if (gamma >= 0){
                b = 255;
            }
            fout << r << " " << g << " " << b << "\n";
        }
    }
    fout.close();
    return 0;
}
