#include <math.h>
#include <stdlib.h>
#include <iostream>

template<typename T>
class vec3 {
    public:
    vec3() {}
    vec3(T e0, T e1, T e2) {e[0] = e0; e[1] = e1; e[2] = e2;}
    inline T r() const {return this-> e[0];}
    inline T g() const {return this->e[1];}
    inline T b() const {return this->e[2];}

    private:
    T e[3];
};