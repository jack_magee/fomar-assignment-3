#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include "vec2.h"
#include "vec3.h"

using namespace std;

bool isInTri(vec2<float> coords, vec3<vec2<float>> triangle)
{
    vec2<float> p1 = triangle.r();
    vec2<float> p2 = triangle.g();
    vec2<float> p3 = triangle.b();

    float s1, s2, s3;
    bool neg, pos;
    
    s1 = (coords.x() - p2.x())*(p1.y() - p2.y()) - (p1.x() - p2.x())*(coords.y() - p2.y());
    s2 = (coords.x() - p3.x())*(p2.y() - p3.y()) - (p2.x() - p3.x())*(coords.y() - p3.y());
    s3 = (coords.x() - p1.x())*(p3.y() - p1.y()) - (p3.x() - p1.x())*(coords.y() - p1.y());

    neg = (s1 < 0) || (s2 < 0) || (s3 < 0);
    pos = (s1 > 0) || (s2 > 0) || (s3 > 0);

    return !(neg && pos);
}

float distance(vec2<float> point, vec2<float> A, vec2<float> B)
{
    float top = (B.x() - A.x())*(A.y()-point.y()) - (A.x()- point.x())*(B.y() - A.y());
    float bot = sqrt((B.x()-A.x())*(B.x()-A.x()) + (B.y()-A.y())*(B.y()-A.y()));
    return top/bot;
}

int main(int argc, char** argv)
{
    int nx = 128;
    int ny = 128;
    vec2<float> A = vec2<float>(61,10);
    vec2<float> B = vec2<float>(100,100);
    vec2<float> C = vec2<float>(25,90);
    vec3<vec2<float>> triangle = vec3<vec2<float>>(A,B,C);

    ofstream fout;
    fout.open("abg.ppm");
    fout << "P3\n" << nx << " " << ny << "\n255\n";

for(int j = ny-1; j >= 0; j--){
    for(int i = 0; i < nx; i++){
            vec2<float> coords = vec2<float>(i,j);
            int r,g,b;
            if(isInTri(coords, triangle)){
                float alpha = (distance(coords, C,B) / distance(A, C,B));
                float beta = (distance(coords, A,C) / distance(B, A,C));
                float gamma = (distance(coords, B,A) / distance(C, B,A));
                r = 100 + int(100 * alpha);
                g = 100 + int(100 * beta);
                b = 100 + int(100 * gamma);
            }
            else{
                r = 255;
                g = 255;
                b = 192;
            }
            fout << r << " " << g << " " << b << "\n";
        }
    }
    fout.close();
    return 0;
}
