#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include "vec2.h"
#include "vec3.h"
#include <iomanip>

using namespace std;

float distance(vec2<float> point, vec2<float> A, vec2<float> B)
{
    float top = (B.x() - A.x())*(A.y()-point.y()) - (A.x()- point.x())*(B.y() - A.y());
    float bot = sqrt((B.x()-A.x())*(B.x()-A.x()) + (B.y()-A.y())*(B.y()-A.y()));
    return top/bot;
}

vec3<float> interpolate(float pu, vec3<float> colour1,  vec3<float> colour2)
{
    float ratio = pu - floor(pu);
    float r = colour1.r()*(1-ratio) + colour2.r()*ratio;
    float g = colour1.g()*(1-ratio) + colour2.g()*ratio;
    float b = colour1.b()*(1-ratio) + colour2.b()*ratio;
    return vec3<float>(round(r),round(b),round(g));
}

int main(int argc, char** argv)
{
    char buffer[100];
    FILE* f = fopen("earth.ppm", "r");
    fgets(buffer, 100, f);
    fgets(buffer, 100, f);
    fgets(buffer, 100, f);
    fgets(buffer, 100, f);

    float val;
    float image[512][256][3];
    for (int j = 0; j < 256; j++){
        for (int i = 0; i < 512; i++){
            for (int k = 0; k < 3; k++){
                fgets(buffer, 100, f);
                sscanf(buffer, "%f", &val);
                image[i][j][k] = val;
            }
        }
    }
    fclose(f);

    int nx = 128;
    int ny = 128;
    vec2<float> A = vec2<float>(61,10);
    vec2<float> Auv = vec2<float>(0.160268,0.290086);
    vec2<float> B = vec2<float>(100,100);
    vec2<float> Buv = vec2<float>(0.083611,0.159907);
    vec2<float> C = vec2<float>(25,90);
    vec2<float> Cuv = vec2<float>(0.230169, 0.222781);
    vec3<vec2<float>> triangle = vec3<vec2<float>>(A,B,C);

    ofstream fout;
    fout.open("texture1.ppm");
    fout << "P3\n" << nx << " " << ny << "\n255\n";

    for(int j = ny-1; j >= 0; j--){
    for(int i = 0; i < nx; i++){
            vec2<float> coords = vec2<float>(i,j);
            int r,g,b;
            float alpha = (distance(coords, C,B) / distance(A, C,B));
            float beta = (distance(coords, A,C) / distance(B, A,C));
            float gamma = (distance(coords, B,A) / distance(C, B,A));
            
            if(alpha < 0 || beta <0 || gamma <0){
                r = 0;
                g = 0;
                b = 0;
            }
            else{
                float Pu = alpha*Auv.x() + beta*Buv.x() + gamma*Cuv.x();
                float Pv = alpha*Auv.y() + beta*Buv.y() + gamma*Cuv.y();

                if (coords.x() == 62 && coords.y() == 65){
                    
                std::cout << std::fixed;
                std::cout << std::setprecision(6);
                    cout << 512*Pu << " " << 256*Pv << "\n";
                }
                Pu = 512*Pu;
                Pv = 256*Pv;

                int PuFloor = floor(Pu);
                int PuCeil = ceil(Pu);
                int PvFloor = floor(Pv);
                int PvCeil = ceil(Pv);

                // cout << Pu << " " << Pv << "\n";
                // cout << PuFloor << PuCeil << PvFloor << PvCeil << "\n";

                vec3<float> q1 = vec3<float>(image[PuFloor][PvFloor][0],image[PuFloor][PvFloor][1],image[PuFloor][PvFloor][2]);
                vec3<float> q2 = vec3<float>(image[PuCeil][PvFloor][0],image[PuCeil][PvFloor][1],image[PuCeil][PvFloor][2]);
                vec3<float> q3 = vec3<float>(image[PuFloor][PvCeil][0],image[PuFloor][PvCeil][1],image[PuFloor][PvCeil][2]);
                vec3<float> q4 = vec3<float>(image[PuCeil][PvCeil][0],image[PuCeil][PvCeil][1],image[PuCeil][PvCeil][2]);
                vec3<float> PuColour1 = interpolate(Pu,q1,q2);
                vec3<float> PuColour2 = interpolate(Pu,q3,q4);
                vec3<float> PvColour = interpolate(Pv,PuColour1, PuColour2);

                

                r = PvColour.r();
                g = PvColour.g();
                b = PvColour.b();
                // r = (int)255*alpha;
                // g = (int)255*beta;
                // b = (int)255*gamma;
            }
            
            fout << r << " " << g << " " << b << "\n";
        }
    }
    fout.close();
    return 0;
}
//Final lostaion at (62,65) is Vancouver, port of vancouver