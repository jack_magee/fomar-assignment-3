#include <math.h>
#include <stdlib.h>

template <typename T>
class vec2 {
    public:
    vec2() {}
    vec2(T e0, T e1) {e[0] = e0; e[1] = e1;}
    inline T x() const {return e[0];}
    inline T y() const {return e[1];}

    private:
    T e[2];
};